import cv2
import numpy as np
import pickle
from sklearn.neural_network import MLPClassifier
from sklearn import svm
import os
import time
import sys

imgsize = 70


def getDescriptor(img, winsize, blocksize, cellsize, blockstride, nbins,
                  deriv_aperture=1, winsigma=-1, histogram_norm_type = 0, l2hysthreshold=0.2,
                  gamma_correction=1, nlevels=64):
    # Calculate histogram of gradients
    hog = cv2.HOGDescriptor(winsize, blocksize, blockstride, cellsize, nbins, deriv_aperture,
                            winsigma, histogram_norm_type, l2hysthreshold, gamma_correction,
                            nlevels)
    descriptor = hog.compute(img)
    return descriptor


def saveFeature(classifier, image_dir, label='BaoBui'):
    train_dataset = None
    train_label = []
    test_dataset = None
    test_label = []
    print len(os.listdir(image_dir))
    for index, item in enumerate(os.listdir(image_dir)):
        print 'Processing picture %d' % (index + 1)

        # Read the image
        image = cv2.imread(image_dir + '/' + item)

        # Grayscale image
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        # Detect faces in the image
        faces = classifier.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(30, 30),
            flags=cv2.cv.CV_HAAR_SCALE_IMAGE
        )

        # Draw a rectangle around the faces
        print 'Found %d faces' % len(faces)
        for (x, y, w, h) in faces:

            # Get face region
            face_region = gray[y:y+h, x:x+w]

            # Resize face
            face_region = cv2.resize(face_region, (imgsize, imgsize), interpolation=cv2.INTER_CUBIC)
            descriptors = getDescriptor(face_region, winsize=(imgsize, imgsize), blocksize=(20, 20), blockstride=(5, 5),
                                            cellsize=(10, 10), nbins=9)
            descriptors = descriptors.T
            if index < len(os.listdir(image_dir)):
                train_label.append(label)
                if train_dataset is None:
                    train_dataset = descriptors
                else:
                    if len(train_dataset.shape) == 2:
                        train_dataset = np.array([train_dataset, descriptors])
                    else:
                        train_dataset = np.vstack((train_dataset, descriptors[np.newaxis, ...]))
            else:
                test_label.append(label)
                if test_dataset is None:
                    test_dataset = descriptors
                else:
                    if len(test_dataset.shape) == 2:
                        test_dataset = np.array([test_dataset, descriptors])
                    else:
                        test_dataset = np.vstack((test_dataset, descriptors[np.newaxis, ...]))


    # Save to pickle file
    pickle_file = label + '.pickle'
    file = open(pickle_file, 'wb')
    data = {}
    data['train_dataset'] = train_dataset
    data['train_label'] = train_label
    data['test_dataset'] = test_dataset
    data['test_label'] = test_label
    pickle.dump(data, file, protocol=pickle.HIGHEST_PROTOCOL)
    file.close()


def loadData(inputfile):
    file = open(inputfile, 'rb')
    data = pickle.load(file)
    file.close()
    return data


def SVM_Linear_Kernel(train_X, train_Y, test_X, test_Y, hypaC, validation=True):
    start = time.time()
    lin_clf = svm.SVC(kernel='linear', C=hypaC, decision_function_shape='ovo', probability=True)
    lin_clf.fit(train_X, train_Y)
    print 'Training time is: %f' % (time.time() - start)
    if validation:
        # Training error
        train_result = lin_clf.predict(train_X)
        train_error = np.mean(train_result!=train_Y)
        print "Error on traing set is: %f" % train_error

        # Out of sample error
        test_result = lin_clf.predict(test_X)
        test_error = np.mean(test_result!=test_Y)
        accuracy = 100 - test_error * 100
        print 'Accuracy is: %f' % accuracy

    return lin_clf


def guessFace(classifier, image_path, face_cascade):
    guess_img = cv2.imread(image_path)

    # Grayscale image
    gray = cv2.cvtColor(guess_img, cv2.COLOR_BGR2GRAY)

    # Detect faces in the image
    faces = face_cascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags=cv2.cv.CV_HAAR_SCALE_IMAGE
    )

    print 'Found %d faces' % len(faces)
    for (x, y, w, h) in faces:
        face_region = gray[y:y+h, x:x+w]
        face_region = cv2.resize(face_region, (imgsize, imgsize), interpolation=cv2.INTER_CUBIC)
        descriptors = getDescriptor(face_region, winsize=(imgsize, imgsize), blocksize=(20, 20), blockstride=(5, 5),
                                            cellsize=(10, 10), nbins=9)
        # Draw rectangle around the face
        cv2.rectangle(guess_img, (x, y), (x+w, y+h), (0, 255, 0), 2)
        cv2.putText(guess_img, classifier.predict(descriptors.T)[0], (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX,
                    1, (0, 255, 0), thickness=2)

    cv2.imshow('Guess faces', guess_img)
    cv2.waitKey(0)

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Wrong parameters"
        exit()

    # Get guess image
    guess_image = sys.argv[1]

    # Create the haar cascade
    cascPath = "haarcascade_frontalface_default.xml"
    faceCascade = cv2.CascadeClassifier(cascPath)

    # Get data
    data1 = loadData('BaoBui.pickle')
    data2 = loadData('CaoDung.pickle')
    data3 = loadData('Stranger.pickle')

    # Append data by stacking them
    train_dataset = np.vstack((data1['train_dataset'], data2['train_dataset']))
    train_dataset = np.vstack((train_dataset, data3['train_dataset']))
    train_label = data1['train_label'] + data2['train_label'] + data3['train_label']
    test_dataset = np.vstack((data1['test_dataset'], data2['test_dataset']))
    test_label = data1['test_label'] + data2['test_label']

    # Reshape training data to have 2 dimension
    train_dataset = train_dataset.reshape(-1, train_dataset.shape[2])
    test_dataset = test_dataset.reshape(-1, test_dataset.shape[2])

    # Train the classifier
    clf = SVM_Linear_Kernel(train_dataset, train_label, test_dataset, test_label, hypaC=10,
                            validation=False)

    # Guess the face
    guessFace(clf, guess_image, faceCascade)



