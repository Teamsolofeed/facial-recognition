## Facial Recognition using SVM ##

### Getting started ###
* The program uses **Haar** algorithms for face detection and **Histogram of Oriented Gradients** to extract features for classification.
* The program uses **SVM** methods to recognize faces between three classes: BaoBui, CaoDung and Stranger.

### Prerequisites ###
* Python 2.7 and pip

### Requirements ###
* Pip install packages numpy, scikit-learn and OpenCV2. For example, open your command line and type:

	pip install numpy

### Run the code ###
* Open your command line and type:

	python face_recognition.py <img_dir>
	
* <img_dir> is the image that you want to recognize

### Results ###
* An input image with the green bounding boxes around faces and the name of each face on top of each box.
![Result](Images/Result.png)